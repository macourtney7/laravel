changelog:

OpenCV Python:
  /py/img_hash.py

Controllers:
  /site/app/Http/Controllers/IndexController.php

Models:
  /site/app/Image.php

Migrations:
  /site/database/migrations/2020_06_01_172922_create_images_table.php

Views:
  /site/resources/views/master.blade.php
  /site/resources/views/index.blade.php

Routes:
  /site/routes/web.php
