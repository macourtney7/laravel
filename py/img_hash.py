import cv2 as cv
import sys, hashlib

class IMG:
    def __init__(self,img):
        self.hash = self.__hash(cv.imread(img))

    def __hash(self,imread):
        return hashlib.sha224(str(imread).encode('utf-8')).hexdigest()

#runtime
img = IMG(sys.argv[1])
print(img.hash)
