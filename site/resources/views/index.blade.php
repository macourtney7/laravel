@extends('master')

@section('content')
  <form action='/image_upload' method='post' enctype='multipart/form-data'>
    <input type='file' name='image'>
    {{ csrf_field() }}
    <input type='submit' value='submit'>
  </form>
  <br>
  stored images:<br>
  @foreach($imgs as $img)
    {{ $img->uhid }} - {{ $img->created_at }} <br>
  @endforeach
@endsection
