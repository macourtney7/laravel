<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

use App\Image;

class IndexController extends Controller
{
    public function page_load()
    {
      return view('index',['imgs'=>Image::all()]);
    }

    public function image_upload(Request $request)
    {
      $img = $request->file('image');
      $img_path = "/var/www/laravel/site/storage/app/".$img->store('images');

      $py_script = "/var/www/laravel/py/img_hash.py";

      #gen img hash
      $process = new Process(['python3', $py_script, $img_path]);
      $process->run();
      if (!$process->isSuccessful()) { throw new ProcessFailedException($process); }

      #add hash to db
      $upload = new Image;
      $upload->uhid = $process->getOutput();
      $upload->save();

      return redirect('/');
    }
}
